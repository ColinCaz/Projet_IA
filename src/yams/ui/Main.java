package yams.ui;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
	protected final static int frameWidth = 350;
	protected final static int frameHeight = 250;

	private JFrame frame;

	public Main() {
		this.doGraphics();
	}

	public JFrame getFrame() {
		return this.frame;
	}

	public void doGraphics() {
		this.frame = new JFrame("Projet d'IA - Yams");
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setSize(Main.frameWidth, Main.frameHeight);

		Panel p = new Panel(this, Main.frameWidth, Main.frameHeight);
		panel.add(p);

		frame.add(panel);
		frame.getContentPane();
		frame.setSize(Main.frameWidth, Main.frameHeight);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new Main();
	}
}