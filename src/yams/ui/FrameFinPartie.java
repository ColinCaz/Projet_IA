package yams.ui;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import yams.jeu.Yams;

public class FrameFinPartie {
	protected final static int frameWidth = 350;
	protected final static int frameHeight = 300;

	private JFrame frame;
	private Yams yams;
	private int score;

	public FrameFinPartie(Yams yams, int score) {
		this.yams = yams;
		this.score = score;
		this.doGraphics();
	}

	public JFrame getFrame() {
		return this.frame;
	}

	public Yams getYams() {
		return this.yams;
	}

	public int getScore() {
		return this.score;
	}

	public void doGraphics() {
		this.frame = new JFrame("Partie terminee");
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setSize(FrameFinPartie.frameWidth, FrameFinPartie.frameHeight);

		PanelFinPartie pfp = new PanelFinPartie(this, FrameFinPartie.frameWidth, FrameFinPartie.frameHeight);
		panel.add(pfp);

		frame.add(panel);
		frame.getContentPane();
		frame.setSize(FrameFinPartie.frameWidth, FrameFinPartie.frameHeight);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}