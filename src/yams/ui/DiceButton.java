package yams.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import yams.jeu.Dice;

public class DiceButton extends JButton {
	private static final long serialVersionUID = 1L;

	protected int w;
	protected int h;
	protected int r;

	protected Color borderColor;
	protected Color selectedBorderColor;

	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	protected Color numberNormalColor;
	protected Color numberPressedColor;
	protected Color numberOverColor;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	private Dice dice;

	public DiceButton(Dice dice) {
		this.dice = dice;
		this.w = FrameYams.frameWidth / 11;
		this.h = FrameYams.frameWidth / 11;
		this.r = FrameYams.frameWidth / 44;
		this.borderColor = new Color(85, 58, 130);
		this.selectedBorderColor = new Color(103, 130, 58);
		this.normalColor = Color.WHITE;
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = new Color(103, 130, 58);
		this.numberNormalColor = Color.BLACK;
		this.numberPressedColor = Color.WHITE;
		this.numberOverColor = Color.WHITE;
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(0, 0, w, h);
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.dice.toggle();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		drawNumberCentered(g, dice.getNumber());
	}

	public void drawNumberCentered(Graphics g, int number) {
		if (mousePressed) {
			g.setColor(this.numberPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.numberOverColor);
			} else {
				g.setColor(this.numberNormalColor);
			}
		}
		switch (number) {
		case 1:
			g.fillOval((int) ((double) this.getWidth() * 3 / 7), (int) ((double) this.getHeight() * 3 / 7),
					this.getWidth() / 7, this.getHeight() / 7);
			break;
		case 2:
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			break;
		case 3:
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 7), (int) ((double) this.getHeight() * 3 / 7),
					this.getWidth() / 7, this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			break;
		case 4:
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			break;
		case 5:
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 7), (int) ((double) this.getHeight() * 3 / 7),
					this.getWidth() / 7, this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			break;
		case 6:
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 7), this.getWidth() / 7, this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 7), this.getWidth() / 7, this.getHeight() / 7);
			g.fillOval((int) ((double) this.getWidth() * 3 / 4 - this.getWidth() / 14),
					(int) ((double) this.getHeight() * 3 / 4 - this.getHeight() / 14), this.getWidth() / 7,
					this.getHeight() / 7);
			break;
		}
	}

	@Override
	protected void paintBorder(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (this.dice.isSelected()) {
			g2D.setColor(this.selectedBorderColor);
			g2D.setStroke(new BasicStroke(3));
			g2D.drawRoundRect(1, 1, this.w - 3, this.h - 3, this.r, this.r);
		} else {
			g2D.setColor(this.borderColor);
			g2D.setStroke(new BasicStroke(1));
			g2D.drawRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		}
		g2D.setStroke(new BasicStroke(1));
	}
}