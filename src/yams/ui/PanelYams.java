package yams.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import javax.swing.JPanel;
import yams.jeu.ScoreGrid.Possibilities;
import yams.jeu.Yams;

public class PanelYams extends JPanel {
	private static final long serialVersionUID = 1L;

	private FrameYams main;
	private Yams yams;
	private ArrayList<DiceButton> diceButtons;

	public PanelYams(FrameYams main, Yams yams, ArrayList<DiceButton> diceButtons, int width, int height) {
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(width, height));
		this.setLayout(null);
		this.main = main;
		this.yams = yams;
		this.diceButtons = diceButtons;
		for (int i = 0; i < this.diceButtons.size(); i++) {
			this.diceButtons.get(i).setLocation((int) (width * (2 * i + 1) / 11), (int) (height * 19) / 26);
			this.add(this.diceButtons.get(i));
		}
		Button button = new Button("Continuer", main, width / 2, height * 23 / 26);
		this.add(button);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (Component c : this.getComponents()) {
			c.paint(g);
		}
		g.clearRect(0, 0, this.getWidth(), this.getHeight() * 19 / 26);
		paintYams(g);
	}

	public String printSiPasMoinsUn(int n) {
		if (n == -1) {
			return "";
		} else {
			return "" + n;
		}
	}

	public void paintYams(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawLine(this.getWidth() / 2, 0, this.getWidth() / 2, this.getHeight() * 7 / 13);
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() / 26, "Somme 1 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_1.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 3 / 26, "Somme 2 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 3 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_2.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 5 / 26, "Somme 3 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 5 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_3.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 7 / 26, "Somme 4 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 7 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_4.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 9 / 26, "Somme 5 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 9 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_5.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 11 / 26, "Somme 6 :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 11 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.SOMME_6.index()]));
		drawStringCentered(g, this.getWidth() / 6, this.getHeight() * 13 / 26, "Bonus :");
		drawStringCentered(g, this.getWidth() / 3, this.getHeight() * 13 / 26, printSiPasMoinsUn(this.yams.getBonus()));

		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() / 26, "Brelan :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.BRELAN.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 3 / 26, "Carre :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 3 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.CARRE.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 5 / 26, "Full House :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 5 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.FULL_HOUSE.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 7 / 26, "Petite suite :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 7 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.PETITE_SUITE.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 9 / 26, "Grande suite :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 9 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.GRANDE_SUITE.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 11 / 26, "Yam's :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 11 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.YAMS.index()]));
		drawStringCentered(g, this.getWidth() * 2 / 3, this.getHeight() * 13 / 26, "Chance :");
		drawStringCentered(g, this.getWidth() * 5 / 6, this.getHeight() * 13 / 26,
				printSiPasMoinsUn(this.yams.getScoreGrid().getGrid()[Possibilities.CHANCE.index()]));

		drawStringCentered(g, this.getWidth() / 2, this.getHeight() * 31 / 52, "Total : " + this.yams.getTotal());

		drawStringCentered(g, this.getWidth() / 2, this.getHeight() * 35 / 52,
				"Selectionnez les des que vous souhaitez garder :");

		drawStringCentered(g, this.getWidth() / 4, this.getHeight() * 23 / 26,
				"Lances restants : " + this.main.getLancesRestants());
	}

	public void drawStringCentered(Graphics g, int x, int y, String text) {
		g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 18));
		Rectangle2D r2D = g.getFont().getStringBounds(text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = x - (rW / 2) - rX;
		int b = y - (rH / 2) - rY;
		g.drawString(text, a, b);
	}
}