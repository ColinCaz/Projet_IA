package yams.ui;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import yams.jeu.Yams;

public class FrameChoix extends JFrame {
	private static final long serialVersionUID = 1L;
	public static int frameWidth = 350;
	public static int frameHeightPerChoice = 50;

	public FrameChoix(Yams yams) {
		super("Faites votre choix");
		this.setSize(frameWidth, frameHeightPerChoice * (yams.getChoix().size() + 1) - 3);
		this.setLocationRelativeTo(yams.getMain().getFrame());
		this.setLocation(yams.getMain().getFrame().getLocation().x + yams.getMain().getFrame().getWidth(),
				yams.getMain().getFrame().getLocation().y);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.getContentPane();
		this.setVisible(true);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, frameWidth, frameHeightPerChoice * yams.getChoix().size());
		panel.setPreferredSize(new Dimension(frameWidth, frameHeightPerChoice * (yams.getChoix().size() + 1)));
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);

		for (int i = 0; i < yams.getChoix().size(); i++) {
			Button choice = new Button(-1, frameHeightPerChoice * i, yams.getChoix().get(i), yams, this);
			panel.add(choice);
		}
		this.add(panel);
	}
}