package yams.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.util.AbstractMap;
import javax.swing.JButton;
import yams.jeu.ScoreGrid.Possibilities;
import yams.jeu.Yams;

public class Button extends JButton {
	private static final long serialVersionUID = 1L;

	protected int w;
	protected int h;
	protected int r;

	protected Color borderColor;
	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	protected String text;
	protected Color textNormalColor;
	protected Color textPressedColor;
	protected Color textOverColor;
	protected int textSize;
	protected String fontName;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	private Main main;
	private FrameYams fy;
	private Yams yams;
	private AbstractMap.SimpleEntry<Possibilities, Integer> choix;
	private FrameChoix fc;
	private FrameFinPartie ffp;
	private boolean b;

	public Button(String text, FrameYams fy, int x, int y) {
		this.fy = fy;
		this.w = 150;
		this.h = 50;
		this.r = 10;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x - w / 2, y - h / 2, w, h);
	}

	public Button(int x, int y, AbstractMap.SimpleEntry<Possibilities, Integer> choix, Yams yams, FrameChoix fc) {
		this.yams = yams;
		this.choix = choix;
		this.fc = fc;
		this.w = 333;
		this.h = 50;
		this.r = 0;
		this.borderColor = Color.BLACK;
		this.normalColor = Color.WHITE;
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = new Color(103, 130, 58);
		this.text = choix.getKey() + " : " + choix.getValue();
		this.textNormalColor = new Color(103, 130, 58);
		this.textPressedColor = Color.WHITE;
		this.textOverColor = Color.WHITE;
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
	}

	public Button(String text, FrameFinPartie ffp, boolean b, int x, int y) {
		this.ffp = ffp;
		this.b = b;
		this.w = 150;
		this.h = 50;
		this.r = 10;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x - w / 2, y - h / 2, w, h);
	}

	public Button(String text, Main main, boolean b, int x, int y) {
		this.main = main;
		this.b = b;
		this.w = 200;
		this.h = 50;
		this.r = 10;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x - w / 2, y - h / 2, w, h);
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				if (this.fy != null) {
					this.fy.click();
				}
				if (this.yams != null) {
					this.yams.choisi(choix.getKey(), choix.getValue());
					this.yams.stopWaiting();
					fc.setVisible(false);
				}
				if (this.ffp != null) {
					if (this.b) {
						this.ffp.getYams().stopWaiting();
						ffp.getFrame().setVisible(false);
					} else {
						ffp.getFrame().dispatchEvent(new WindowEvent(ffp.getFrame(), WindowEvent.WINDOW_CLOSING));
					}
				}
				if (this.main != null) {
					new FrameYams(this.b);
					main.getFrame().setVisible(false);
				}
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		if (this.fy != null || this.ffp != null || this.main != null) {
			g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		}
		if (this.yams != null) {
			g2D.fillRect(0, 0, this.w, this.h);
		}
		drawStringCentered(g);
	}

	public void drawStringCentered(Graphics g) {
		g.setFont(new Font(this.fontName, Font.BOLD, textSize));
		Rectangle2D r2D = g.getFont().getStringBounds(this.text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = (this.w / 2) - (rW / 2) - rX;
		int b = (this.h / 2) - (rH / 2) - rY;
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		g.drawString(this.text, a, b);
	}

	@Override
	protected void paintBorder(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(this.borderColor);
		if (this.fy != null || this.ffp != null || this.main != null) {
			g2D.drawRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		}
		if (this.yams != null) {
			g2D.drawRect(0, 0, this.w, this.h);
		}
	}
}