package yams.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.Scanner;
import javax.swing.JPanel;

public class PanelFinPartie extends JPanel {
	private static final long serialVersionUID = 1L;

	private FrameFinPartie main;
	private int highscore;

	public PanelFinPartie(FrameFinPartie main, int width, int height) {
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(width, height));
		this.setLayout(null);
		this.main = main;
		Button button = new Button("Rejouer", main, true, width / 2, height / 2);
		this.add(button);
		button = new Button("Quitter", main, false, width / 2, 7 * height / 10);
		this.add(button);

		URL path = this.getClass().getProtectionDomain().getCodeSource().getLocation();
		String strPath = path.getPath().substring(1, path.getPath().lastIndexOf('/'));
		String str = "";
		try {
			str = URLDecoder.decode(strPath, "UTF-8");
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}

		File file;
		if (main.getYams().withIA()) {
			file = new File(str + "/highscoreIA.txt");
		} else {
			file = new File(str + "/highscore.txt");
		}
		try {
			if (file.createNewFile()) {
				try {
					PrintWriter output = new PrintWriter(file);
					output.print(0);
					output.close();
				} catch (FileNotFoundException exception) {
					System.out.println(exception);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		file.setWritable(true);
		try {
			Files.setAttribute(file.toPath(), "dos:hidden", false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			@SuppressWarnings("resource")
			Scanner input = new Scanner(file);
			int highscore = input.nextInt();
			if (highscore > main.getScore()) {
				this.highscore = highscore;
			} else {
				this.highscore = main.getScore();
				try {
					PrintWriter output = new PrintWriter(file);
					output.print(main.getScore());
					output.close();
				} catch (FileNotFoundException exception) {
					System.out.println(exception);
				}
			}
		} catch (IOException exception) {
			System.err.println(exception);
		}
		file.setWritable(false);
		try {
			Files.setAttribute(file.toPath(), "dos:hidden", true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (Component c : this.getComponents()) {
			c.paint(g);
		}
		g.clearRect(0, 0, this.getWidth(), this.getHeight() / 4);
		drawStringCentered(g, this.getWidth() / 2, this.getHeight() / 12, "Score : " + main.getScore());
		drawStringCentered(g, this.getWidth() / 2, this.getHeight() / 4, "Highscore : " + this.highscore);
	}

	public void drawStringCentered(Graphics g, int x, int y, String text) {
		g.setColor(Color.BLACK);
		g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 18));
		Rectangle2D r2D = g.getFont().getStringBounds(text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = x - (rW / 2) - rX;
		int b = y - (rH / 2) - rY;
		g.drawString(text, a, b);
	}
}