package yams.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class Panel extends JPanel {
	private static final long serialVersionUID = 1L;

	public Panel(Main main, int width, int height) {
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(width, height));
		this.setLayout(null);
		Button button = new Button("Partie assitee par l'IA", main, true, width / 2, height / 3);
		this.add(button);
		button = new Button("Partie normale", main, false, width / 2, 3 * height / 5);
		this.add(button);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (Component c : this.getComponents()) {
			c.paint(g);
		}
		g.clearRect(0, 0, this.getWidth(), this.getHeight() / 4);
		drawStringCentered(g, this.getWidth() / 2, this.getHeight() / 8, "Jouer une partie de Yams : ");
	}

	public void drawStringCentered(Graphics g, int x, int y, String text) {
		g.setColor(Color.BLACK);
		g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 18));
		Rectangle2D r2D = g.getFont().getStringBounds(text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = x - (rW / 2) - rX;
		int b = y - (rH / 2) - rY;
		g.drawString(text, a, b);
	}
}