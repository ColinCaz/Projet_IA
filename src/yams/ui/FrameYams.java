package yams.ui;

import java.awt.Color;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import yams.jeu.Agent;
import yams.jeu.Dice;
import yams.jeu.Effector;
import yams.jeu.Sensor;
import yams.jeu.Yams;

public class FrameYams {
	protected static final int frameWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 80;
	protected static final int frameHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 80;

	private JFrame frame;
	private JPanel jPanel;
	private PanelYams panel;
	private Yams yams;
	private Agent agent;
	private ArrayList<DiceButton> diceButtons;
	private boolean ia;

	public FrameYams(boolean ia) {
		this.yams = new Yams(this, ia);
		this.diceButtons = new ArrayList<DiceButton>();
		for (Dice d : this.yams.getDices()) {
			this.diceButtons.add(new DiceButton(d));
		}
		this.agent = new Agent(new Sensor(this.yams), new Effector(this.yams));
		this.ia = ia;
		this.doGraphics();
	}

	public JFrame getFrame() {
		return this.frame;
	}

	public int getLancesRestants() {
		return this.yams.getLancesRestants();
	}

	public void click() {
		this.yams.next();
		this.frame.repaint();
	}

	public void repaint() {
		this.frame.repaint();
	}

	public void doGraphics() {
		this.frame = new JFrame("Partie de Yams" + (ia ? " assistee par l'IA" : ""));
		this.jPanel = new JPanel();
		jPanel.setBackground(Color.WHITE);
		jPanel.setSize(FrameYams.frameWidth, FrameYams.frameHeight);

		this.panel = new PanelYams(this, this.yams, this.diceButtons, FrameYams.frameWidth, FrameYams.frameWidth);
		jPanel.add(this.panel);

		frame.add(jPanel);
		frame.getContentPane();
		frame.setSize(FrameYams.frameWidth, FrameYams.frameHeight);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new FrameYams(true);
	}

	public Agent getAgent() {
		return this.agent;
	}
}