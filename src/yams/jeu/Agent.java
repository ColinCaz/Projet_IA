package yams.jeu;

import java.util.AbstractMap;
import java.util.ArrayList;
import yams.jeu.ScoreGrid.Possibilities;
import yams.network.Element;
import yams.network.Network;

public class Agent {
	private Sensor sensor;
	private Effector effector;
	private ArrayList<Network> networks;

	public Agent(Sensor _sensor, Effector _effector) {
		this.networks = new ArrayList<Network>();
		this.sensor = _sensor;
		this.effector = _effector;
		for (int i = 0; i <= Yams.nbDice; i++) {
			this.networks.add(new Network(i, this.sensor.getScoreGrid()));
		}
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public Effector getEffector() {
		return effector;
	}

	public void setEffector(Effector effector) {
		this.effector = effector;
	}

	public void choose() {
		for (Network n : this.networks) {
			n.setRoot(this.sensor.getResultDices());
			n.setScoreGrid(this.sensor.getScoreGrid());
		}
		ArrayList<Element> maxs = new ArrayList<Element>();
		System.out.println("Actuellement : ");
		for (Network n : this.networks) {
			System.out.println(n.getBestPossibility().getPossibility());
			System.out.println(n.getBestPossibility().getScore());
			maxs.add(n.getBestPossibility());
		}
		Element max = maxs.get(0);
		for (Element e : maxs)
			if (max.getScore() < e.getScore())
				max = e;
		this.effector.selectDices(max.getDiceToRoll());
		System.out.println();
	}

	public void choosePossibility(ArrayList<AbstractMap.SimpleEntry<Possibilities, Integer>> possibilities) {
		AbstractMap.SimpleEntry<Possibilities, Integer> result = possibilities.get(0);
		for (AbstractMap.SimpleEntry<Possibilities, Integer> e : possibilities) {
			result = e.getValue() >= result.getValue() ? e : result;
			// superieur OU EGAL pour respecter l'ordre : le brelan se trouve avant le carre
			// et peuvent etre �gaux, il faut priviligi� le carr�
		}
		this.effector.choosePossibility(result);
	}
}