package yams.jeu;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import yams.jeu.ScoreGrid.Possibilities;

public class Effector {
	private Yams yams;

	public Effector(Yams yams) {
		this.yams = yams;
	}

	public void selectDices(ArrayList<Integer> dices) {
		System.out.println(dices);
		for (int i = 0; i < this.yams.getDices().size(); i++) {
			if (dices.contains(i + 1)) {
				if (this.yams.getDices().get(i).isSelected()) {
					this.yams.getDices().get(i).toggle();
				}
			} else {
				if (!this.yams.getDices().get(i).isSelected()) {
					this.yams.getDices().get(i).toggle();
				}
			}
		}
	}

	public void choosePossibility(SimpleEntry<Possibilities, Integer> result) {
		this.yams.choisi(result.getKey(), result.getValue());
		this.yams.stopWaiting();
	}
}