package yams.jeu;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import yams.jeu.ScoreGrid.Possibilities;
import yams.tool.Tool;
import yams.ui.FrameChoix;
import yams.ui.FrameFinPartie;
import yams.ui.FrameYams;

public class Yams {
	public static final int nbDice = 5;

	private FrameYams main;
	private ScoreGrid scoreGrid;
	private ArrayList<Dice> dices;
	private ArrayList<AbstractMap.SimpleEntry<Possibilities, Integer>> possibilities;

	private int lancesRestants = 3;

	private boolean ia;
	private boolean wait = false;

	public Yams(FrameYams main, boolean ia) {
		this.main = main;
		this.scoreGrid = new ScoreGrid();
		this.dices = new ArrayList<Dice>();
		for (int i = 0; i < nbDice; i++) {
			this.dices.add(new Dice());
		}
		this.ia = ia;
	}

	public FrameYams getMain() {
		return this.main;
	}

	public void reset() {
		this.main.getFrame().setEnabled(false);
		this.main.getFrame().setVisible(false);
		this.wait = true;
		new FrameFinPartie(this, this.getTotal());
		for (Dice d : this.dices) {
			d.reset();
		}
		Integer[] grid = new Integer[13];
		for (int i = 0; i < 13; i++) {
			grid[i] = -1;
		}
		this.scoreGrid.setGrid(grid);
		this.scoreGrid.setTotal(0);
		this.lancesRestants = 3;
	}

	public int getLancesRestants() {
		return this.lancesRestants;
	}

	public void next() {
		if (!wait) {
			if (!fini()) {
				if (this.lancesRestants > 0) {
					int count = 0;
					for (Dice d : this.dices) {
						if (d.isSelected()) {
							count++;
						}
					}
					if (count == 5) {
						nouvelleTentative();
					} else {
						nouveauLance();
					}
				} else {
					nouvelleTentative();
				}
			} else {
				this.reset();
			}
		}
	}

	public void select() {
		if (this.ia) {
			this.main.getAgent().choose();
		}
	}

	public void nouveauLance() {
		if (this.lancesRestants > 0) {
			for (Dice d : this.dices) {
				if (!d.isSelected()) {
					d.roll();
				} else {
					d.toggle();
				}
			}
		}
		this.lancesRestants--;
		if (this.lancesRestants != 0) {
			this.select();
		} else if (!this.ia) {
			this.nouvelleTentative();
		}
	}

	public void nouvelleTentative() {
		if (this.dices.get(0).getNumber() != 0) {
			this.main.getFrame().setEnabled(false);
			this.wait = true;
			evaluer();
		}
	}

	public ArrayList<AbstractMap.SimpleEntry<Possibilities, Integer>> getChoix() {
		return this.possibilities;
	}

	public void choisi(Possibilities possibilities, int i) {
		AbstractMap.SimpleEntry<Possibilities, Integer> choisi = new AbstractMap.SimpleEntry<Possibilities, Integer>(
				possibilities, i);
		this.scoreGrid.getGrid()[choisi.getKey().index()] = choisi.getValue();
		for (Dice d : this.dices) {
			d.reset();
		}
		this.lancesRestants = 3;
		main.repaint();
	}

	public void stopWaiting() {
		this.main.getFrame().setVisible(true);
		this.main.getFrame().setEnabled(true);
		this.wait = false;
	}

	public void evaluer() {
		this.possibilities = new ArrayList<AbstractMap.SimpleEntry<Possibilities, Integer>>();
		ArrayList<Integer> diceValues = new ArrayList<Integer>();
		for (Dice d : this.dices) {
			diceValues.add(d.getNumber());
		}
		diceValues.sort(null);

		Integer[] grid = this.scoreGrid.getGrid();
		for (Possibilities p : Possibilities.values()) {
			if (grid[p.index()] == -1) {
				if (p.index() == Possibilities.YAMS.index()) {
					int value = Tool.verifyOccurences(diceValues, 5) ? p.value() : 0;
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p, value));
				} else if (p.index() == Possibilities.PETITE_SUITE.index()) {
					int value = Tool.verifySuite(4, diceValues) ? p.value() : 0;
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p, value));
				} else if (p.index() == Possibilities.GRANDE_SUITE.index()) {
					int value = Tool.verifySuite(5, diceValues) ? p.value() : 0;
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p, value));
				} else if (p.index() == Possibilities.FULL_HOUSE.index()) {
					int value = Tool.verifyDoubleOccurences(diceValues, 3, 2) ? p.value() : 0;
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p, value));
				} else if (p.index() == Possibilities.CHANCE.index())
					this.possibilities.add(
							new AbstractMap.SimpleEntry<Possibilities, Integer>(p, Tool.getTotalFromDices(diceValues)));
				else if (p.index() > Possibilities.SOMME_6.index()) {
					int value = Tool.verifyOccurences(diceValues, p.value()) ? Tool.getTotalFromDices(diceValues) : 0;
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p, value));
				} else
					this.possibilities.add(new AbstractMap.SimpleEntry<Possibilities, Integer>(p,
							Tool.count(diceValues, p.value()) * p.value()));
			}
		}
		if (this.possibilities.size() > 0) {
			if (ia) {
				this.main.getAgent().choosePossibility(this.possibilities);
			} else {
				new FrameChoix(this);
			}
		} else {
			this.reset();
		}
	}

	public int getBonus() {
		int total = 0;
		for (int i = 0; i < 6; i++) {
			total += this.scoreGrid.getGrid()[i];
		}
		if (total > 62) {
			return 35;
		} else {
			return 0;
		}
	}

	public int siPasMoinsUn(int n) {
		if (n == -1) {
			return 0;
		} else {
			return n;
		}
	}

	public int getTotal() {
		int total = 0;
		int index = 0;
		for (int v : this.scoreGrid.getGrid()) {
			total += v == -1 ? 0 : v;
			index++;
			if (index == 6) {
				total += total > 62 ? 35 : 0;
			}
		}
		this.scoreGrid.setTotal(total);
		return total;
	}

	public boolean fini() {
		return (Collections.frequency(new ArrayList<Integer>(Arrays.asList(this.scoreGrid.getGrid())), -1) == 0);
	}

	public ArrayList<Dice> getDices() {
		return dices;
	}

	public void setDices(ArrayList<Dice> dices) {
		this.dices = dices;
	}

	public ScoreGrid getScoreGrid() {
		return this.scoreGrid;
	}

	public boolean withIA() {
		return this.ia;
	}
}