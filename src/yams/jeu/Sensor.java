package yams.jeu;

import java.util.ArrayList;

public class Sensor {
	private Yams yams;

	public Sensor(Yams yams) {
		this.yams = yams;
	}

	public ArrayList<Integer> getResultDices() {
		ArrayList<Integer> valueDices = new ArrayList<Integer>();
		for (Dice d : this.yams.getDices())
			valueDices.add(d.getNumber());
		return valueDices;
	}

	public ScoreGrid getScoreGrid() {
		return this.yams.getScoreGrid();
	}
}