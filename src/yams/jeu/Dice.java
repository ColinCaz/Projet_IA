package yams.jeu;

import java.util.Random;

public class Dice {
	public final static int FACES = 6;

	private int number;
	private boolean selected;

	public Dice() {
		this.reset();
	}

	public void reset() {
		this.selected = false;
		this.number = 0;
	}

	public int getNumber() {
		return this.number;
	}

	public boolean isSelected() {
		return this.selected;
	}

	public void roll() {
		Random rand = new Random();
		this.number = rand.nextInt(FACES) + 1;
	}

	public void toggle() {
		if (this.number != 0) {
			this.selected = !this.selected;
		}
	}

	public String toString() {
		return "" + this.number;
	}
}