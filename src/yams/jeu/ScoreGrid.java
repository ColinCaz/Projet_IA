package yams.jeu;

public class ScoreGrid {
	private Integer[] grid;

	private int total = 0;

	public enum Possibilities {
		SOMME_1(0, 1), SOMME_2(1, 2), SOMME_3(2, 3), SOMME_4(3, 4), SOMME_5(4, 5), SOMME_6(5, 6), CHANCE(9, 1),
		BRELAN(6, 3), CARRE(7, 4), FULL_HOUSE(8, 25), PETITE_SUITE(10, 30), GRANDE_SUITE(11, 40), YAMS(12, 50);

		private int index;
		private int value;

		Possibilities(int i, int v) {
			this.index = i;
			this.value = v;
		}

		public int index() {
			return this.index;
		}

		public int value() {
			return this.value;
		}
	}

	public ScoreGrid() {
		this.grid = new Integer[Possibilities.values().length];
		for (int i = 0; i < 13; i++) {
			this.grid[i] = -1;
		}
	}

	public Integer[] getGrid() {
		return grid;
	}

	public void setGrid(Integer[] grid) {
		this.grid = grid;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}