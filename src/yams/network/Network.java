package yams.network;

import java.util.ArrayList;
import yams.jeu.Dice;
import yams.jeu.ScoreGrid;
import yams.jeu.Yams;

public class Network {
	private int ndr; // number of dice to reroll
	private Element root;
	private ScoreGrid scoreGrid;
	private float probability;

	public Network(int n, ScoreGrid _scoreGrid) {
		this.ndr = n;
		this.scoreGrid = _scoreGrid;
		this.probability = (float) Math.pow(1.0f / Dice.FACES, this.ndr);
		this.root = new Element(new ArrayList<Integer>(), new ArrayList<Integer>(), this.scoreGrid, this.probability,
				null);
	}

	public void setRoot(ArrayList<Integer> _root) {
		this.root.setPossibility(_root);
		this.root.evalueWeight();
	}

	public void setScoreGrid(ScoreGrid sc) {
		this.scoreGrid = sc;
	}

	public ArrayList<ArrayList<Integer>> getRerollPossibilities() {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		recursiveCombination(result, new int[this.ndr], 1, (Yams.nbDice), 0);
//		System.out.println("-----------------------------------");
//		for (ArrayList<Integer> combination : result) {
//			System.out.println(combination);
//		}
//		System.out.println("-----------------------------------");
		return result;
	}

	private void recursiveCombination(ArrayList<ArrayList<Integer>> combinations, int data[], int start, int end,
			int index) {
		if (index == data.length) {
			ArrayList<Integer> combination = new ArrayList<Integer>();
			for (int i : data) {
				combination.add(i);
			}
			combinations.add(combination);
		} else if (start <= end) {
			data[index] = start;
			recursiveCombination(combinations, data, start + 1, end, index + 1);
			recursiveCombination(combinations, data, start + 1, end, index);
		}
	}

	public ArrayList<Element> getAllPossibilities() {
		ArrayList<Element> result = new ArrayList<Element>();
		ArrayList<ArrayList<Integer>> combinations = getRerollPossibilities();
		for (ArrayList<Integer> combination : combinations) {
			result.addAll(getPossibilities(combination));
		}
		return result;
	}

	public ArrayList<Element> getPossibilities(ArrayList<Integer> combination) {
		ArrayList<Element> result = new ArrayList<Element>();
		result.addAll(recur(new ArrayList<Integer>(this.root.getPossibility()), 0, combination));
//		for (Element e : result) {
//			System.out.println(e.getPossibility());
//		}
//		System.out.println(result.size());
		return result;
	}

	private ArrayList<Element> recur(ArrayList<Integer> possibilities, int lvl, ArrayList<Integer> combination) {
		ArrayList<Element> result = new ArrayList<Element>();
		for (int j = 1; j <= Dice.FACES; j++) {
			ArrayList<Integer> p = new ArrayList<Integer>(possibilities);
			p.set(combination.get(lvl) - 1, j);
			if (lvl + 1 < this.ndr) {
				result.addAll(recur(p, lvl + 1, combination));
			} else {
				result.add(new Element(p, combination, this.scoreGrid, this.probability, this.root));
			}
		}
		return result;
	}

	private ArrayList<Element> clearPossibilities() {
		ArrayList<Element> possibilities = getAllPossibilities();
		ArrayList<Element> result = new ArrayList<Element>();
		for (Element e : possibilities) {
			if (!isContaining(result, e)) {
				result.add(e);
			}
		}
		return result;
	}

	private boolean isContaining(ArrayList<Element> array, Element element) {
		ArrayList<Integer> sortElement = new ArrayList<Integer>(element.getPossibility());
		sortElement.sort(null);
		for (Element e : array) {
			ArrayList<Integer> sortE = new ArrayList<Integer>(e.getPossibility());
			sortE.sort(null);
			if (sortE.equals(sortElement)) {
				e.incrOccurences();
				return true;
			}
		}
		return false;
	}

	public Element getBestPossibility() {
		if (this.ndr == 0) {
			Element e = new Element(this.root.getPossibility(), new ArrayList<Integer>(), this.scoreGrid,
					this.probability, this.root);
			e.evalueWeight();
			return e;
		}
		ArrayList<Element> elements = clearPossibilities();
		for (Element e : elements) {
			e.evalueWeight();
		}
		Element best = elements.get(0);
		for (Element e : elements) {
//			System.out.println("tttttttttttt");
//			System.out.println(e.getDiceToRoll());
//			System.out.println(e.getScore());
//			System.out.println(e.getPossibility());
//			System.out.println(e.getWeight());
			if (e.getScore() > best.getScore()) {
				best = e;
			}
		}
		return best;
	}

	public static void main(String[] args) {
		Network n = new Network(0, new ScoreGrid());
		// int[] r = new int[] { 5, 3, 6, 4, 1 };
		int[] r = new int[] { 2, 5, 4, 1, 6 };
		ArrayList<Integer> root = new ArrayList<Integer>();
		root.add(r[0]);
		root.add(r[1]);
		root.add(r[2]);
		root.add(r[3]);
		root.add(r[4]);
		n.setRoot(root);
		Element e = n.getBestPossibility();
		System.out.println("$$$$$$$$$$$$$$$$$$$");
		System.out.println(n.root.getPossibility());
		System.out.println(e.getPossibility());
		System.out.println(e.getDiceToRoll());
		System.out.println(e.getOccurences());
		System.out.println(e.getWeight());
		System.out.println(e.getProbability());
		System.out.println(e.getScore());
	}
}