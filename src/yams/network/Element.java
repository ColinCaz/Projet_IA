package yams.network;

import java.util.ArrayList;
import yams.jeu.ScoreGrid;
import yams.jeu.ScoreGrid.Possibilities;
import yams.tool.Tool;

public class Element {
	private ArrayList<Integer> possibility;
	private ArrayList<Integer> diceToRoll;
	private float probability;
	private int occurences;
	private float weight;
	private ScoreGrid scoreGrid;
	private Element root;

	public Element(ArrayList<Integer> _possibility, ArrayList<Integer> _diceToRoll, ScoreGrid _scoreGrid, float proba,
			Element root) {
		this.scoreGrid = _scoreGrid;
		this.occurences = 1;
		this.probability = proba;
		this.root = root;
		this.setPossibility(_possibility);
		this.setDiceToRoll(_diceToRoll);
	}

	public ArrayList<Integer> getPossibility() {
		return this.possibility;
	}

	public void setPossibility(ArrayList<Integer> possibility) {
		this.possibility = possibility;
		this.weight = 0;
	}

	public ArrayList<Integer> getDiceToRoll() {
		return diceToRoll;
	}

	public void setDiceToRoll(ArrayList<Integer> diceToRoll) {
		this.diceToRoll = diceToRoll;
	}

	public float getProbability() {
		return probability;
	}

	public void setProbability(float probability) {
		this.probability = probability;
	}

	public int getOccurences() {
		return this.occurences;
	}

	public void setOccurences(int o) {
		this.occurences = o;
	}

	public void incrOccurences() {
		this.occurences++;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public void evalueWeight() {
		// la liste possibility est d�j� tri�e
		ArrayList<Integer> allWeights = new ArrayList<Integer>();
		Integer[] grid = this.scoreGrid.getGrid();
		for (Possibilities p : Possibilities.values()) {
			if (grid[p.index()] == -1) {
				if (p.index() == Possibilities.YAMS.index()) {
					allWeights.add(Tool.verifyOccurences(this.possibility, 5) ? p.value() : 0);
				} else if (p.index() == Possibilities.PETITE_SUITE.index())
					allWeights.add(Tool.verifySuite(4, this.possibility) ? p.value() : 0);
				else if (p.index() == Possibilities.GRANDE_SUITE.index())
					allWeights.add(Tool.verifySuite(5, this.possibility) ? p.value() : 0);
				else if (p.index() == Possibilities.FULL_HOUSE.index())
					allWeights.add(Tool.verifyDoubleOccurences(this.possibility, 3, 2) ? p.value() : 0);
				else if (p.index() == Possibilities.CHANCE.index())
					allWeights.add(Tool.getTotalFromDices(this.possibility));
				else if (p.index() > Possibilities.SOMME_6.index())
					allWeights.add(Tool.verifyOccurences(this.possibility, p.value())
							? Tool.getTotalFromDices(this.possibility)
							: 0);
				else
					allWeights.add(Tool.count(this.possibility, p.value()) * p.value());
			}
		}
		allWeights.sort(null);
		if (this.root == null)
			for (float f : allWeights)
				this.weight += f;
		else if (this.diceToRoll.isEmpty()) {
			this.weight = ((grid[Possibilities.GRANDE_SUITE.index()] == -1 && Tool.verifySuite(5, this.possibility))
					|| (grid[Possibilities.FULL_HOUSE.index()] == -1
							&& Tool.verifyDoubleOccurences(this.possibility, 3, 2))) ? Integer.MAX_VALUE : 0;
		} else {
			for (float f : allWeights)
				this.weight += f;
			this.weight = (this.weight - this.root.getWeight()) / allWeights.size();
		}
	}

	public double getScore() {
		// return this.weight * this.occurences * this.probability;
		return this.weight * this.probability;
	}
}