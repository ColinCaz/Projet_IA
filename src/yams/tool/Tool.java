package yams.tool;

import java.util.ArrayList;
import java.util.Collections;
import yams.jeu.Dice;

public class Tool {
	public static boolean verifyOccurences(ArrayList<Integer> values, int occur) {
		for (int i : values)
			if (Collections.frequency(values, i) > occur - 1)
				return true;
		return false;
	}

	public static boolean verifyDoubleOccurences(ArrayList<Integer> values, int occur1, int occur2) {
		for (int i : values)
			if (Collections.frequency(values, i) > occur1 - 1)
				for (int j : values)
					if (i != j && Collections.frequency(values, j) > occur2 - 1)
						return true;
		return false;
	}

	public static boolean verifySuite(int v, ArrayList<Integer> values) {
		for (int i = 1; i < Dice.FACES + 1; i++) {
			int n = i;
			boolean b = true;
			for (int j = 0; j < v; j++) {
				if (values.contains(n)) {
					n += 1;
				} else {
					b = false;
					break;
				}
			}
			if (b)
				return b;
		}
		return false;
	}

	public static int getTotalFromDices(ArrayList<Integer> values) {
		int result = 0;
		for (int v : values) {
			result += v;
		}
		return result;
	}

	public static int count(ArrayList<Integer> values, int n) {
		int count = 0;
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i) == n) {
				count++;
			}
		}
		return count;
	}
}